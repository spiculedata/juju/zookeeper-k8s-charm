#!/usr/bin/env python3
# Copyright 2020 Tom Barber
# See LICENSE file for licensing details.

import logging

import logging
from typing import Dict, Iterable, List
from ops.charm import CharmBase
from ops.main import main
from ops.framework import StoredState
from charmhelpers.core import host, hookenv
import yaml
import ops.main
import ops.model

logger = logging.getLogger(__name__)


class ZookeeperCharm(CharmBase):
    _stored = StoredState()

    def __init__(self, *args):
        super().__init__(*args)
        self.framework.observe(self.on.config_changed, self._on_config_changed)
        self.framework.observe(self.on.fortune_action, self._on_fortune_action)
        self._stored.set_default(things=[])

    def _on_config_changed(self, _):
        goal_state = hookenv.goal_state()

        logger.info("Goal state <<EOM\n{}\nEOM".format(yaml.dump(goal_state)))

            # Only the leader can set_spec().
        spec = self.make_pod_spec()
        resources = self.make_pod_resources()

        msg = "Configuring pod"
        logger.info(msg)
        self.model.unit.status = ops.model.MaintenanceStatus(msg)
        self.model.pod.set_spec(spec, {"kubernetesResources": resources})

        msg = "Pod configured"
        logger.info(msg)
        self.model.unit.status = ops.model.ActiveStatus(msg)

    def _on_fortune_action(self, event):
        fail = event.params["fail"]
        if fail:
            event.fail(fail)
        else:
            event.set_results({"fortune": "A bug in the code is worth two in the documentation."})

    def make_pod_spec(self) -> Dict:
        config = self.model.config
        env_config = {}
        env_config["ZOO_ENABLE_AUTH"] = config["enableAuth"]
        env_config["ALLOW_ANONYMOUS_LOGIN"] = config["anonymousLogin"]
        image_details = {
                "imagePath": config["image"],
        }

        ports =  [
                {"name": "zookeeper", "containerPort": 2181, "protocol": "TCP"},
            ]
        spec = {
                "version": 3,
                "kubernetesResources":{
                    "pod":{
                        "securityContext":{
                            "fsGroup": 1001,
                            "runAsUser": 1001,
                            "runAsGroup":1001,
                        }
                    }
                },
                "containers": [
                    {
                        "name": self.app.name,
                        "imageDetails": image_details,
                        "imagePullPolicy": "Always",
                        "ports": ports,
                        "envConfig": env_config,
                    },
                ],
            }

        logger.info(f"Pod spec <<EOM\n{yaml.dump(spec)}\nEOM")
        return spec

    def make_pod_resources(self) -> Dict:
        secrets_data = {}

        services = [
            {
                "name": "zookeeper-port",
                "spec": {
                    "type": "NodePort",
                    "clusterIP": "",
                    "ports": [{"name": "zookeeper", "port": 2181, "protocol": "TCP"}],
                },
            },
        ]

        resources = {
            "secrets": [{"name": "zookeeper-secrets", "type": "Opaque", "data": secrets_data}],
            "services": services,
        }
        logger.info(f"Pod resources <<EOM\n{yaml.dump(resources)}\nEOM")

        return resources

if __name__ == "__main__":
    main(ZookeeperCharm)
